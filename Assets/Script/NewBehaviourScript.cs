using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.DrawRay(transform.position, Vector2.right, Color.black, 1);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right);
           if (hit.collider.CompareTag("Enemigo"))
            {
                Destroy(hit.collider.gameObject);
            }
        }
    }
}
